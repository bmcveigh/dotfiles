# Load .bashrc file to make the CLI color-coded.
source ~/.bashrc

# Exports to $PATH variable.
export PATH=$PATH:/Applications/PhpStorm.app/Contents/MacOS
export PATH="$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
