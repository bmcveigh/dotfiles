## Description

Provides private Unix files for a local environment setup. Please note that this was only tested on Mac.

## Questions you may have

#### What do the files include?

* Adds color-coding to the CLI.
* Adds aliases for git.

#### Where should I put all these files?

Make sure to place them in your home directory.
